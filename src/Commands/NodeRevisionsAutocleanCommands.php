<?php

namespace Drupal\node_revisions_autoclean\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node_revisions_autoclean\Batch\Batch;
use Drupal\node_revisions_autoclean\Services\RevisionsManager;
use Drush\Commands\DrushCommands;
use Drupal\Core\Batch\BatchBuilder;

/**
 * Class NodeRevisionsAutocleanCommands.
 *
 * @package Drupal\node_revisions_autoclean\Commands
 */
class NodeRevisionsAutocleanCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManager.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\node_revisions_autoclean\Services\RevisionsManager.
   *
   * @var Drupal\node_revisions_autoclean\Services\RevisionsManager
   */
  protected $revisionsManager;

  /**
   * NodeRevisionsAutocleanCommands constructor.
   *
   * @param Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   EntityTypeManager.
   * @param Drupal\node_revisions_autoclean\Services\RevisionsManager $revisionsManager
   *   RevisionsManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RevisionsManager $revisionsManager) {
    parent::__construct();
    $this->entityTypeManager = $entityTypeManager;
    $this->revisionsManager = $revisionsManager;
  }

  /**
   * Deletes old revisions according to site's settings.
   *
   * @command nra-delete-old-revisions
   * @validate-module-enabled node
   * @aliases nra:dor
   */
  public function deleteRevisionsAccordingSiteSettings() {
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple();
    $count = 0;
    foreach ($nodes as $node) {
      $revisions = $this->revisionsManager->revisionsToDelete($node);
      if (count($revisions)) {
        $this->revisionsManager->deleteRevisions($revisions);
        $this->logger()
          ->log('success', $this->t('@count revisions deleted for node @nid : @label', [
            '@count' => count($revisions),
            '@nid' => $node->id(),
            '@label' => $node->label(),
          ]));
      }
      $count += count($revisions);
    }
    $this->logger()
      ->log('success', $this->t('Global : @count revisions deleted.', [
        '@count' => $count,
      ]));
  }

  /**
   * Deletes old revisions according to site's settings - batch.
   *
   * @command nra-delete-old-revisions-batch
   * @validate-module-enabled node
   * @aliases nra:dorb
   */
  public function deleteRevisionsAccordingSiteSettingsBatch() {
    $this->logger()->info('Revision deletion batch operations start');
    try {
      $storage = $this->entityTypeManager->getStorage('node');
      $query = $storage->getQuery()
        ->condition('status', '1');
      $nids = $query->execute();
    } catch (\Exception $e) {
      $this->logger()->error($e->getMessage());
    }

    $operations = [];
    $numOperations = 0;
    $batchId = 1;

    // Defining the batch builder.
    $batch_builder = new BatchBuilder();
    $batch_builder->setFinishCallback([Batch::class, 'processNodeFinished']);

    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $this->logger()
          ->info($this->t("Preparing batch : @id ", ['@id' => $batchId]));
        // Adding the operation.
        $batch_builder->addOperation(
          [Batch::class, 'processNode'],
          [$batchId, $nid]
        );
        $batchId++;
        $numOperations++;
      }
    }
    else {
      $this->logger()->warning('No nodes');
    }
    // Setting the title.
    $batch_builder->setTitle($this->t('Node revisions autoclean  @num node(s)', ['@num' => $numOperations]));

    // Setting the batch.
    batch_set($batch_builder->toArray());

    drush_backend_batch_process();
    $this->logger()->info("Batch operations end.");
  }
}
