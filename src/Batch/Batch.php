<?php

namespace Drupal\node_revisions_autoclean\Batch;

use Drupal\node\Entity\Node;

class Batch {

  public function processNode($id, $nid, &$context) {
    $node = Node::load($nid);
    /* @var $revisionsManager \Drupal\node_revisions_autoclean\Services\RevisionsManager */
    $revisionsManager = \Drupal::service('node_revisions_autoclean.revisions_manager');
    $revisions = $revisionsManager->revisionsToDelete($node);
    $revisionsManager->deleteRevisions($revisions);

    $context['results'][] = $id;
    $context['message'] = t('Running Batch node revisions autoclean "@id" on node @nid',
      ['@id' => $id, '@nid' => $nid]
    );
  }

  /**
   * Batch Finished callback.
   *
   * @param bool $success
   *   Success of the operation.
   * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public function processNodeFinished($success, array $results, array $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('Node revisions autoclean : @count nodes processed.', ['@count' => count($results)]));
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(
        t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }
}
