Node revisions autoclean

Description
===========
Automatically cleanup revision history of nodes.

Requirements
============

* Drupal 8.x (drush 9 for drush commands) 
* Node module

Installation
============
* Copy the 'node_revisions_autoclean' module directory 
in to your Drupal 'modules'directory as usual.
* Enable the module using drush or UI

Advanced Settings
=================

You may administer module there : 
/admin/config/content/revisions-autoclean
